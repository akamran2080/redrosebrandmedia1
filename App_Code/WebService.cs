﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }
    [WebMethod(true)]
    public string EmailSending(string sName, string sEmail, string sSubject, string sComment)
    {
        string sJsonString = "{\"retCode\":\"0\"}";
        if (EmailManager.SendEmail("faisalnensey@gmail.com","Enquiry", "Information Received",sName, sEmail, sSubject, sComment) == true)//info@tmauto.in
        {
            sJsonString = "{\"retCode\":\"1\",}";
        }
        return sJsonString;
    }
}

